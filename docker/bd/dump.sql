SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


CREATE DATABASE cadastrobasico WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.utf8' LC_CTYPE = 'pt_BR.utf8';

  

ALTER DATABASE cadastrobasico OWNER TO postgresadmin;

\connect cadastrobasico

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE SCHEMA sch_cadastro AUTHORIZATION postgresadmin;

SELECT schema_name     FROM information_schema.schemata    WHERE schema_name NOT LIKE 'pg_%' AND schema_name != 'information_schema';



CREATE TABLE sch_cadastro.t_produto (
	id_produto bigserial NOT NULL,	 
	nome varchar(60) NOT NULL,
	valor numeric(15,2) NOT NULL,
	disponivel boolean NOT NULL,
	data_criacao timestamp NOT NULL DEFAULT now(),
	data_alteracao timestamp NOT NULL DEFAULT now(),		
	CONSTRAINT pk_produto PRIMARY KEY (id_produto)
);


CREATE SEQUENCE sch_cadastro.t_produto_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 69
	CACHE 1
	NO CYCLE;

	
INSERT INTO sch_cadastro.t_produto	(nome, valor, disponivel, data_criacao, data_alteracao) VALUES ('Camisa', 15.99, true, now(), now());
INSERT INTO sch_cadastro.t_produto	(nome, valor, disponivel, data_criacao, data_alteracao) VALUES ('Camisa 89898989899898', 15.99, true, now(), now());
INSERT INTO sch_cadastro.t_produto	(nome, valor, disponivel, data_criacao, data_alteracao) VALUES ('Camisa 37488', 15.99, true, now(), now());

select * from sch_cadastro.t_produto;

 