package com.br.springcadastrobasico.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.br.springcadastrobasico.entity.ProdutoEntity;
import com.br.springcadastrobasico.repository.ProdutoRepository;
import com.br.springcadastrobasico.vo.ProdutoVo;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public List<ProdutoEntity> listar() throws Exception {

		try {

			List<ProdutoEntity> lista = this.produtoRepository.findAll();
  
			return lista;

		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public void salvar(ProdutoVo produto) throws Exception {

		try {

			ProdutoEntity entityProduto = mapperProdutoEntity(produto);
			this.produtoRepository.save(entityProduto);

		} catch (Exception e) {
			throw new Exception(e);
		}

	}

	public ProdutoEntity pesquisa(Long codigo) throws Exception {
		Optional<ProdutoEntity> produto = this.produtoRepository.findById(codigo);

		try {

			if (!produto.isPresent()) {
				throw new ObjectNotFoundException(codigo, "codigo");
			}

			return produto.get();

		} catch (ObjectNotFoundException e) {
			throw new ObjectNotFoundException(e.getMessage(), null);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public void editar(ProdutoVo produto) {

		Optional<ProdutoEntity> produtoEntity = this.produtoRepository.findById(produto.getIdProduto());

		if (produtoEntity.isPresent()) {
			ProdutoEntity entity = produtoEntity.get();
			this.produtoRepository.save(mapperProdutoEntity(produto, entity));
		}
	}

	public void deletar(Long codigo) throws Exception {

		try {

			this.produtoRepository.deleteById(codigo);

		} catch (EmptyResultDataAccessException e) {
			throw new EmptyResultDataAccessException(e.getMessage(), 0);

		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	private ProdutoEntity mapperProdutoEntity(ProdutoVo produto) {

		ProdutoEntity entity = new ProdutoEntity();

		entity.setNome(produto.getNome());
		entity.setValor(produto.getValor());
		entity.setDisponivel(produto.getDisponivel());
		entity.setDataCriacao(LocalDateTime.now());
		entity.setDataAlteracao(LocalDateTime.now());

		return entity;
	}

	private ProdutoEntity mapperProdutoEntity(ProdutoVo vo, ProdutoEntity entity) {

		entity.setIdProduto(vo.getIdProduto());
		entity.setNome(vo.getNome());
		entity.setValor(vo.getValor());
		entity.setDisponivel(vo.getDisponivel());
		entity.setDataAlteracao(LocalDateTime.now());

		return entity;
	}

}
