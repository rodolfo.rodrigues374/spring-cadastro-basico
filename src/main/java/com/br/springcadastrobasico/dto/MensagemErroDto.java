package com.br.springcadastrobasico.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensagemErroDto {

	public MensagemErroDto(String descricao,String url) {
		this.descricao = descricao;
		this.url = url;
	}

	private String descricao;

	private String url; 
}
