package com.br.springcadastrobasico.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "t_produto")
public class ProdutoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tProdutoSeq")
	@SequenceGenerator(name = "tProdutoSeq", sequenceName = "t_produto_seq", allocationSize = 1)
	@Column(name = "id_produto")
	private Long idProduto;
 
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "valor")
	private BigDecimal valor;
	
	@Column(name = "disponivel")
	private Boolean disponivel;
	
	@Column(name = "data_criacao")
	private LocalDateTime dataCriacao;
	
	@Column(name = "data_alteracao")
	private LocalDateTime dataAlteracao;
}
