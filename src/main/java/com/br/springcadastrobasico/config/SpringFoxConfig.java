package com.br.springcadastrobasico.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration 
public class SpringFoxConfig {
 

	@Bean
	public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption,
			@Value("${application-version}") String appVersion) {

		return new OpenAPI()
						.info(new Info()
						.title("Spring Cadastro Básico REST API")
						.version(appVersion)
						.description(appDesciption)
//						 .contact(new Contact( "Rodolfo Rodrigues", "http://rr.com.br", "rodolfo.rodrigues374@gmail.com"))
						.termsOfService("http://swagger.io/terms/")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));

	}
}
