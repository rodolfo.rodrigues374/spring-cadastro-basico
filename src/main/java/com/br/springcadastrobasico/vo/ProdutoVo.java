package com.br.springcadastrobasico.vo;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoVo {

	@Schema(description = "Código do produto.", example = "123", required = true)
	private Long idProduto;

	@Size(min = 2, max = 100, message = "Nome min 2 e max 100 caracteres")
	@NotEmpty(message = "Nome obrigatório")
	@NotBlank(message = "Nome obrigatório")
	@Schema(description = "Nome do produto.", example = "Teste 01", required = true)
	private String nome;

	@NotNull(message = "Valor obrigatório")
	@Schema(description = "Valor do produto.", example = "1.99", required = true)
	private BigDecimal valor;

	@Schema(description = "Disponibilidade do produto.", example = "true ou false", required = true)
	private Boolean disponivel;
}
