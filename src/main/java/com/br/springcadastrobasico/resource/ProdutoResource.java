package com.br.springcadastrobasico.resource;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.springcadastrobasico.dto.MensagemErroDto;
import com.br.springcadastrobasico.entity.ProdutoEntity;
import com.br.springcadastrobasico.service.ProdutoService;
import com.br.springcadastrobasico.vo.ProdutoVo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/produto")
@SuppressWarnings("rawtypes")
@Tag(name = "produto", description = "Produto API")
public class ProdutoResource extends BaseResource {

	@Autowired
	private ProdutoService produtoService;

	@Operation(summary = "Retorna uma lista de produtos", description = "Essa operação retorna as informações de produtos.", tags = {
			"produto" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de produtos retornada com sucesso.", 
					content = @Content(array = @ArraySchema(schema = @Schema(implementation = ProdutoEntity.class)))) })
	@RequestMapping(value = "/listar", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity listarProdutos(HttpServletRequest req) {

		try {
			List<ProdutoEntity> lista = this.produtoService.listar();
			return new ResponseEntity<List<ProdutoEntity>>(lista, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Salva o produto", description = "Essa operação salva as informações de produtos.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Produto salvo com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ProdutoEntity.class)))) })
	@RequestMapping(value = "/salvar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity salvarProduto(@RequestBody @Valid ProdutoVo produto, HttpServletRequest req) {

		try {
			this.produtoService.salvar(produto);

			return new ResponseEntity<Void>(HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Pesquisa o produto", description = "Essa operação pesquisa as informações do produto código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Produto pesquisado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ProdutoEntity.class)))) })
	@RequestMapping(value = "/pesquisa/{codigo}", method = RequestMethod.GET)
	public ResponseEntity pesquisaProduto(
			@Parameter(name = "codigo", description = "Codígo do produto.", example = "123", required = true) @PathVariable Long codigo,
			HttpServletRequest req) {
		try {
			ProdutoEntity produto = this.produtoService.pesquisa(codigo);
			return new ResponseEntity<ProdutoEntity>(produto, HttpStatus.OK);
		} catch (ObjectNotFoundException e) {
			return new ResponseEntity<String>(e.getIdentifier().toString(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Operation(summary = "Alteração do produto", description = "Essa operação altera as informações do produto código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Produto alerado com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ProdutoEntity.class)))) })
	@RequestMapping(value = "/editar", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editaProduto(@RequestBody ProdutoVo produto, HttpServletRequest req) {

		try {
			this.produtoService.editar(produto);

			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (ObjectNotFoundException e) {
			return new ResponseEntity<String>(e.getMessage().toString(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Eclusão do produto", description = "Essa operação excluí o produto por código.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Produto excluído com sucesso.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ProdutoEntity.class)))) })
	@RequestMapping(value = "/excluir/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deletarProduto(
			@Parameter(name = "codigo", description = "Codígo do produto.", example = "123", required = true) @PathVariable Long codigo,
			HttpServletRequest req) {

		try {
			this.produtoService.deletar(codigo);

			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}