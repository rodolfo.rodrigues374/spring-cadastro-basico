package com.br.springcadastrobasico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.br.springcadastrobasico.config.ApplicationPropertyConfig;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationPropertyConfig.class)
public class SpringCadastroBasicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCadastroBasicoApplication.class, args);
	}

}
