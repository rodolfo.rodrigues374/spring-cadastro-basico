package com.br.springcadastrobasico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.springcadastrobasico.entity.ProdutoEntity;
 
@Repository
public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {

}
