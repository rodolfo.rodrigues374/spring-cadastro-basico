# spring-cadastro-basico


REST API with Spring Boot using CRUD.

  - Swagger
  - Docker
  - PostgreSQL
  - Kubernetes


http://localhost:8080/cadastro-basico/swagger-ui.html

minikube service app-spring-cadastro-basico -n devops --url
http://192.168.49.2:30165/cadastro-basico/swagger-ui.html

minikube ip
http://192.168.49.2/cadastro-basico/swagger-ui.html



### Delete info kub.

```sh
kubectl delete -n devops service app-spring-cadastro-basico &&
kubectl delete -n devops service db-spring-cadastro-basico  &&
kubectl delete -n devops deployment app-spring-cadastro-basico &&
kubectl delete -n devops deployment db-spring-cadastro-basico &&
kubectl delete -n devops persistentvolumeclaim pvc-spring-cadastro-basico &&
kubectl delete -n devops configmap configmap-spring-cadastro-basico &&
kubectl delete -n devops secret secret-spring-cadastro-basico &&
kubectl delete namespace devops &&
kubectl delete persistentvolume pv-spring-cadastro-basico  
```


### Criando e publicando a imagem docker da aplicação.
Criando uma imagem da aplicação de forma automática. 

```sh
$ mvn clean package dockerfile:build
```

Publicando a imagem no docker hub.

```sh
$ docker push rodolfo374/spring-cadastro-basico:1.0
```

### Criando e publicando a imagem do banco Postgres da aplicação.
Criando uma imagem postgres com database, schema, table e uma carga inicial ja definido.

```sh
$ docker-compose -f docker/bd/postgres.yml up -d
```

Publicando a imagem no docker hub.
```sh
$ docker push rodolfo374/postgres-spring-cadastro-basico:1.0
```

### Criando os objetos do kubernetes compartilhado entre banco e aplicação.


Criando o namespace.

```sh
$ kubectl apply -f kubernetes/share/namespace.yml
```

Criando o configmap.

```sh
$ kubectl apply -f kubernetes/share/configmap.yml
```
Criando o secret.

```sh
$ kubectl apply -f kubernetes/share/secret.yml
```




### Criando os objetos do kubernetes do banco de dados.


Criando o volume.

```sh
$ kubectl apply -f kubernetes/bd/postgres-volume.yml
```
Criando o volume claim.

```sh
$ kubectl apply -f kubernetes/bd/postgres-volume-claim.yml
```
Criando o service.

```sh
$ kubectl apply -f kubernetes/bd/postgres-service.yml
```
Criando o deployment.

```sh
$ kubectl apply -f kubernetes/bd/postgres-deployment.yml
```
 
### Criando os objetos do kubernetes da aplicação.


Criando o service.

```sh
$ kubectl apply -f kubernetes/app/app-service.yml

```
Criando o deployment.

```sh
$ kubectl apply -f kubernetes/app/app-deployment.yml
```
